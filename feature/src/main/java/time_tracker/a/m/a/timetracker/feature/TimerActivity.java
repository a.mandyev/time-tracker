package time_tracker.a.m.a.timetracker.feature;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TimerActivity extends AppCompatActivity {

    public static final int MINUTE = 60_000;
    public static final int HOUR = 60 * MINUTE;
    private static final String ADJUST_BUTTON_ADJUST = "Adjust";
    private static final String ADJUST_BUTTON_SAVE = "Done";
    public static final String STATE_FILE_NAME = "TimerTrackerState";

    private Chronometer chronometer;
    private ToggleButton timerToggle;
    private LinearLayout adjustLayout;
    private Button adjustButton;
    private TextView adjustLabel;
    private long timeOff = 0;
    private long adjustmentMs = 1_000;
    private boolean timerRunning = false;
    private boolean adjustingTimer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        chronometer = findViewById(R.id.chronometer);
        chronometer.stop();
        timerToggle = findViewById(R.id.timerToggle);
        adjustLayout = findViewById(R.id.adjustLayout);
        adjustButton = findViewById(R.id.adjustButton);
        adjustLabel = findViewById(R.id.adjustLabel);
        SeekBar seekBar = findViewById(R.id.seekBar);
        adjustButton.setText(ADJUST_BUTTON_ADJUST);
        seekBar.setOnSeekBarChangeListener(new TimeAdjustSeekBarListener());

        onStart();
        refreshAdjustLabel();
        // TODO: ama save and restore chronometer state
    }

    @Override
    protected void onStart() {
        System.out.println("### on start");
        super.onStart();
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
            getApplicationContext().openFileInput(STATE_FILE_NAME)))) {
            TimerState state = (TimerState) ois.readObject();
            timeOff = state.getTimeOff();
            timerRunning = state.isTimerRunning();
            if (timerRunning) {
                chronometer.setBase(state.getBase());
                chronometer.start();
            } else {
                chronometer.setBase(SystemClock.elapsedRealtime() - timeOff);
                chronometer.stop();
            }
            timerToggle.setChecked(state.isTimerRunning());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        System.out.println("### on stop");
        super.onStop();
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(
            getApplicationContext().openFileOutput(STATE_FILE_NAME, MODE_PRIVATE)))) {
            oos.writeObject(new TimerState(chronometer.getBase(), timeOff, timerRunning));
        } catch (IOException e) {
            // TODO: ama add proper logs
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void switchTimer(View view) {
        if (!timerRunning) {
            chronometer.setBase(SystemClock.elapsedRealtime() - timeOff);
            chronometer.start();
            timerRunning = true;
        } else {
            timeOff = SystemClock.elapsedRealtime() - chronometer.getBase();
            chronometer.stop();
            timerRunning = false;
        }
    }

    public void adjustTimer(View view) {
        if (!adjustingTimer) {
            refreshAdjustLabel();
            adjustLayout.setVisibility(View.VISIBLE);
            adjustButton.setText(ADJUST_BUTTON_SAVE);
            adjustingTimer = true;
        } else {
            adjustLayout.setVisibility(View.INVISIBLE);
            adjustButton.setText(ADJUST_BUTTON_ADJUST);
            adjustingTimer = false;
        }
    }

    public void addTime(View view) {
        switchTimer(null);
        timeOff += adjustmentMs;
        chronometer.setBase(SystemClock.elapsedRealtime() - timeOff);
        refreshAdjustLabel();
        switchTimer(null);
    }

    public void removeTime(View view) {
        switchTimer(null);
        timeOff -= adjustmentMs;
        chronometer.setBase(SystemClock.elapsedRealtime() - timeOff);
        refreshAdjustLabel();
        switchTimer(null);
    }

    public void resetTime(View view) {
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        timeOff = 0;
        timerToggle.setChecked(false);
        timerRunning = false;
    }

    @SuppressLint("DefaultLocale")
    private void refreshAdjustLabel() {
        if (adjustmentMs < HOUR) {
            adjustLabel.setText(String.format("%d m", adjustmentMs / MINUTE));
        } else {
            adjustLabel.setText(String.format("%d h", adjustmentMs / HOUR));
        }
    }

    private class TimeAdjustSeekBarListener implements OnSeekBarChangeListener {

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (progress < 10) {
                adjustmentMs = MINUTE;
            } else if (progress < 20) {
                adjustmentMs = 2 * MINUTE;
            } else if (progress < 30) {
                adjustmentMs = 5 * MINUTE;
            } else if (progress < 40) {
                adjustmentMs = 10 * MINUTE;
            } else if (progress < 50) {
                adjustmentMs = 30 * MINUTE;
            } else if (progress < 60) {
                adjustmentMs = HOUR;
            } else if (progress < 70) {
                adjustmentMs = 2 * HOUR;
            } else if (progress < 80) {
                adjustmentMs = 4 * HOUR;
            } else {
                adjustmentMs = 8 * HOUR;
            }
            refreshAdjustLabel();
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
