package time_tracker.a.m.a.timetracker.feature;

import java.io.Serializable;

public class TimerState implements Serializable {

    private final long base;
    private final long timeOff;
    private final boolean timerRunning;

    public TimerState(long base, long timeOff, boolean timerRunning) {
        this.base = base;
        this.timeOff = timeOff;
        this.timerRunning = timerRunning;
    }

    public long getBase() {
        return base;
    }

    public long getTimeOff() {
        return timeOff;
    }

    public boolean isTimerRunning() {
        return timerRunning;
    }
}
